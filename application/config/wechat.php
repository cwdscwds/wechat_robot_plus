<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * TOKEN
 */
$config['token'] = '';

/*
 * 命令超时时间设置
 */
$config['cmd_expire_time'] = 300;

/*
 * 命令标识符,当用户发送的文本中含有本符号则判断为命令,命令格式为[指令][分隔符][指令值],如kill#123
 * 强烈建议设置为一个正常交流中不常用但是输入比较方便的符号
 */
$config['cmd_sign'] = '#';

/*
 * 帮助信息关键字,作为在数据库中标识帮助信息用
 */
$config['help_keyword'] = 'help_contents';

/*
 * 消息模板,正常情况下无需修改
 */
$config['textTpl'] = "<xml>
							<ToUserName><![CDATA[%s]]></ToUserName>
							<FromUserName><![CDATA[%s]]></FromUserName>
							<CreateTime>%s</CreateTime>
							<MsgType><![CDATA[%s]]></MsgType>
							<Content><![CDATA[%s]]></Content>
							<FuncFlag>0</FuncFlag>
							</xml>";

$config['newsTpl'] = "<xml>
							 <ToUserName><![CDATA[%s]]></ToUserName>
							 <FromUserName><![CDATA[%s]]></FromUserName>
							 <CreateTime>%s</CreateTime>
							 <MsgType><![CDATA[%s]]></MsgType>
							 <ArticleCount>1</ArticleCount>
							 <Articles>
							 <item>
							 <Title><![CDATA[%s]]></Title>
							 <Description><![CDATA[%s]]></Description>
							 <PicUrl><![CDATA[%s]]></PicUrl>
							 <Url><![CDATA[%s]]></Url>
							 </item>
							 </Articles>
							 <FuncFlag>0</FuncFlag>
							 </xml>";

$config['musicTpl'] = "<xml>
							 <ToUserName><![CDATA[%s]]></ToUserName>
							 <FromUserName><![CDATA[%s]]></FromUserName>
							 <CreateTime>%s</CreateTime>
							 <MsgType><![CDATA[%s]]></MsgType>
							 <Music>
							 <Title><![CDATA[%s]]></Title>
							 <Description><![CDATA[%s]]></Description>
							 <MusicUrl><![CDATA[%s]]></MusicUrl>
							 <HQMusicUrl><![CDATA[%s]]></HQMusicUrl>
							 </Music>
							 <FuncFlag>0</FuncFlag>
							 </xml>";

/* End of file wechat.php */
/* Location: ./application/config/wechat.php */